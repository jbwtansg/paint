### Requirements
Java 8
Maven

### Instructions

1. cd {project-dir}
2. mvn clean package;
3. java -jar target/paint-1.0-shaded.jar
