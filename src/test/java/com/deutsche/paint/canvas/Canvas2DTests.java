package com.deutsche.paint.canvas;

import com.deutsche.paint.drawingstyle.DrawingStyle;
import com.deutsche.paint.drawingtool.DrawingTool;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class Canvas2DTests {
    @Test(expected = IllegalArgumentException.class)
    public void whenWidthLessThan1ThenNewCanvasThrowsException(){
        new Canvas2D(-1, 1);
    }
    @Test(expected = IllegalArgumentException.class)
    public void whenHeightLessThan1ThenNewCanvasThrowsException(){
        new Canvas2D(1, -1);
    }
    @Test
    public void whenWidthAndHeightGreaterThan1ThenNewCanvasInitialisesContents(){
        String contents = new Canvas2D(5, 2).toString();
        String[] lines = contents.split("\\n");
        assertEquals("|-----|", lines[0]);
        assertEquals("|     |", lines[1]);
        assertEquals("|     |", lines[2]);
        assertEquals("|-----|", lines[3]);
    }

    @Test
    public void whenCanvasAcceptDrawingToolThenDrawingToolInvoked(){
        DrawingTool tool = Mockito.mock(DrawingTool.class);
        DrawingStyle style = Mockito.mock(DrawingStyle.class);
        Canvas2D canvas = new Canvas2D(5, 2).acceptDraw(tool, style);
        verify(tool, times(1)).draw(canvas, style);
    }
}
