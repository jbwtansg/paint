package com.deutsche.paint.drawingtool;

import com.deutsche.paint.canvas.Canvas2D;
import com.deutsche.paint.drawingstyle.DrawingStyle;
import org.junit.Test;
import org.mockito.Mockito;

import static org.mockito.Matchers.anyChar;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.*;

public class RectangleToolTests {
    @Test(expected = IllegalArgumentException.class)
    public void whenX2LessThanX1ThenNewRectangleToolThrowsException(){
        new RectangleTool(2, 1, 1, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenY2LessThanY1ThenNewRectangleToolThrowsException(){
        new RectangleTool(1, 2, 2, 1);
    }

    @Test
    public void whenRectangleToolDrawThenCanvasFillInvoked2x2(){
        Canvas2D canvas = Mockito.mock(Canvas2D.class);
        DrawingStyle style = Mockito.mock(DrawingStyle.class);
        when(style.getSymbol()).thenReturn('B');
        new RectangleTool(1, 1, 2, 2).draw(canvas, style);

        verify(canvas, times(4)).fill(anyInt(), anyInt(), anyChar());
        verify(canvas, times(1)).fill(1, 1, 'B');
        verify(canvas, times(1)).fill(1, 2, 'B');
        verify(canvas, times(1)).fill(2, 1, 'B');
        verify(canvas, times(1)).fill(2, 2, 'B');
    }

    @Test
    public void whenRectangleToolDrawThenCanvasFillInvoked3x3(){
        Canvas2D canvas = Mockito.mock(Canvas2D.class);
        DrawingStyle style = Mockito.mock(DrawingStyle.class);
        when(style.getSymbol()).thenReturn('B');
        new RectangleTool(1, 1, 3, 3).draw(canvas, style);

        verify(canvas, times(8)).fill(anyInt(), anyInt(), anyChar());
        verify(canvas, times(1)).fill(1, 1, 'B');
        verify(canvas, times(1)).fill(1, 2, 'B');
        verify(canvas, times(1)).fill(1, 3, 'B');
        verify(canvas, times(1)).fill(2, 1, 'B');
        verify(canvas, times(0)).fill(2, 2, 'B');
        verify(canvas, times(1)).fill(2, 3, 'B');
        verify(canvas, times(1)).fill(3, 1, 'B');
        verify(canvas, times(1)).fill(3, 2, 'B');
        verify(canvas, times(1)).fill(3, 3, 'B');
    }
}
