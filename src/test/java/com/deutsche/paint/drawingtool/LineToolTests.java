package com.deutsche.paint.drawingtool;


import com.deutsche.paint.canvas.Canvas2D;
import com.deutsche.paint.drawingstyle.DrawingStyle;
import org.junit.Test;
import org.mockito.Mockito;

import static org.mockito.Matchers.anyChar;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.times;

public class LineToolTests {
    @Test(expected = IllegalArgumentException.class)
    public void whenX2LessThanX1ThenNewLineToolThrowsException(){
        new LineTool(2, 1, 1, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenY2LessThanY1ThenNewLineToolThrowsException(){
        new LineTool(1, 2, 2, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenDiagonalCoordsNewLineToolThrowsException(){
        new LineTool(1, 2, 2, 1);
    }

    @Test
    public void whenLineToolDrawThenCanvasFillInvoked(){
        Canvas2D canvas = Mockito.mock(Canvas2D.class);
        DrawingStyle style = Mockito.mock(DrawingStyle.class);
        when(style.getSymbol()).thenReturn('B');
        new LineTool(1, 1, 2, 1).draw(canvas, style);

        verify(canvas, times(2)).fill(anyInt(), anyInt(), anyChar());
        verify(canvas, times(1)).fill(1, 1, 'B');
        verify(canvas, times(1)).fill(2, 1, 'B');
    }
}
