package com.deutsche.paint.validator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class ApplicationArgsValidatorTests {

    @InjectMocks
    private ApplicationArgsValidator validator;

    @Test
    public void whenArgsEmptyThenValidatorReturnsFalse(){
        assertFalse(validator.isValid(null));
        assertFalse(validator.isValid(new String[0]));
    }
    @Test
    public void whenFirstArgCAndArgLengthIs3AndRemainderArgsNumbersThenValidatorReturnsTrue(){
        assertTrue(validator.isValid(toArray("C", "20", "5")));
    }
    @Test
    public void whenFirstArgCAndArgLengthIs3AndRemainderArgsSignedNumbersThenValidatorReturnsTrue(){
        assertTrue(validator.isValid(toArray("C", "+20", "-5")));
    }
    @Test
    public void whenFirstArgCAndArgLengthIs3AndRemainderArgsNotNumbersThenValidatorReturnsFalse(){
        assertFalse(validator.isValid(toArray("C", "20", "A")));
    }
    @Test
    public void whenFirstArgCAndArgLengthIsLessThanGreater3ThenValidatorReturnsFalse(){
        assertFalse(validator.isValid(toArray("C", "20")));
        assertFalse(validator.isValid(toArray("C", "20", "5", "5")));
    }
    @Test
    public void whenFirstArgLAndArgLengthIs5AndRemainderArgsNumbersThenValidatorReturnsTrue(){
        assertTrue(validator.isValid(toArray("L", "1", "3", "7", "3")));
    }
    @Test
    public void whenFirstArgLAndArgLengthIsLessThanGreater5ThenValidatorReturnsFalse(){
        assertFalse(validator.isValid(toArray("L", "1", "3", "7")));
        assertFalse(validator.isValid(toArray("L", "1", "3", "7", "3", "3")));
    }
    @Test
    public void whenFirstArgLAndArgLengthIs5AndRemainderArgsNotNumbersThenValidatorReturnsFalse(){
        assertFalse(validator.isValid(toArray("L", "1", "3", "A")));
    }
    @Test
    public void whenFirstArgRAndArgLengthIs5AndRemainderArgsNumbersThenValidatorReturnsTrue(){
        assertTrue(validator.isValid(toArray("R", "15", "2", "20", "5")));
    }
    @Test
    public void whenFirstArgRAndArgLengthIsLessThanGreater5ThenValidatorReturnsFalse(){
        assertFalse(validator.isValid(toArray("R", "15", "2", "20")));
        assertFalse(validator.isValid(toArray("R", "15", "2", "20", "5", "5")));
    }
    @Test
    public void whenFirstArgRAndArgLengthIs5AndRemainderArgsNotNumbersThenValidatorReturnsFalse(){
        assertFalse(validator.isValid(toArray("R", "15", "2", "20", "A")));
    }
    @Test
    public void whenFirstArgQThenValidatorReturnsTrue(){
        assertTrue(validator.isValid(toArray("Q")));
    }
    @Test
    public void whenFirstArgUnrecognisedThenValidatorReturnsFalse(){
        assertFalse(validator.isValid(toArray("Z")));
    }

    private String[] toArray(String... elements) {
        String[] array = elements;
        return array;
    }
}
