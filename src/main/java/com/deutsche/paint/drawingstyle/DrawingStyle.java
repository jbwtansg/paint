package com.deutsche.paint.drawingstyle;

public class DrawingStyle {

    private char symbol;

    public DrawingStyle(char symbol) {
        this.symbol = symbol;
    }

    public char getSymbol() {
        return symbol;
    }
}
