package com.deutsche.paint.drawingtool;

import com.deutsche.paint.canvas.Canvas2D;
import com.deutsche.paint.drawingstyle.DrawingStyle;

public interface DrawingTool {
    public void draw(Canvas2D canvas, DrawingStyle style);

}
