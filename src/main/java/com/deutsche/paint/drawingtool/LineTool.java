package com.deutsche.paint.drawingtool;

import com.deutsche.paint.canvas.Canvas2D;
import com.deutsche.paint.drawingstyle.DrawingStyle;

public class LineTool extends LinearDrawingTool {

    private int x1, y1, x2, y2;

    public LineTool(int x1, int y1, int x2, int y2) {
        if (lessThan(x2, x1) || lessThan(y2, y1)) {
            throw new IllegalArgumentException("Invalid bounds for constructing line: x2 must be greater than or " +
                    "equal to x1. y2 must be greater than or equal to y1");
        }
        if (isDiagonalLine(x1, y1, x2, y2)){
            throw new IllegalArgumentException("Sorry. Diagonal lines not supported.");
        }
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
    }

    private boolean isDiagonalLine(int x1, int y1, int x2, int y2) {
        return x1 != x2 && y1 != y2;
    }

    private boolean lessThan(int first, int second) {
        return first < second;
    }

    @Override
    public void draw(Canvas2D canvas, DrawingStyle style) {
        drawLine(canvas, x1, y1, x2, y2, style);
    }
}
