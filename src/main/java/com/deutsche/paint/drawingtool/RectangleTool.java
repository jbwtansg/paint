package com.deutsche.paint.drawingtool;

import com.deutsche.paint.canvas.Canvas2D;
import com.deutsche.paint.drawingstyle.DrawingStyle;

public class RectangleTool extends LinearDrawingTool {

    private int x1, y1, x2, y2;

    public RectangleTool(int x1, int y1, int x2, int y2) {
        if (lessThanEqual(x2, x1) || lessThanEqual(y2, y1)) {
            throw new IllegalArgumentException("Invalid bounds for constructing rectangle: x2 must be greater than x1" +
                    ". y2 must be greater than y1");
        }
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
    }

    private boolean lessThanEqual(int first, int second) {
        return first <= second;
    }

    @Override
    public void draw(Canvas2D canvas, DrawingStyle style) {
        drawLine(canvas, x1, y1, x2 - 1, y1, style);
        drawLine(canvas, x2, y1, x2, y2 - 1, style);
        drawLine(canvas, x1 + 1, y2, x2, y2, style);
        drawLine(canvas, x1, y1 + 1, x1, y2, style);
    }
}
