package com.deutsche.paint.drawingtool;

import com.deutsche.paint.canvas.Canvas2D;
import com.deutsche.paint.drawingstyle.DrawingStyle;

public abstract class LinearDrawingTool implements DrawingTool {
    protected void drawLine(Canvas2D canvas, int x1, int y1, int x2, int y2, DrawingStyle style){
        if(y1 == y2){
            for(int x = x1, y = y1; x <= x2; x++){
                canvas.fill(x, y, style.getSymbol());
            }
        } else if(x1 == x2){
            for(int x = x1, y = y1; y <= y2; y++){
                canvas.fill(x, y , style.getSymbol());
            }
        }
    }
}
