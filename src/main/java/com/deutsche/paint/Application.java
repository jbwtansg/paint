package com.deutsche.paint;

import com.deutsche.paint.canvas.Canvas2D;
import com.deutsche.paint.drawingstyle.DrawingStyle;
import com.deutsche.paint.drawingtool.LineTool;
import com.deutsche.paint.drawingtool.RectangleTool;
import com.deutsche.paint.validator.ApplicationArgsValidator;

import java.util.Arrays;
import java.util.Scanner;

public class Application {

    private ApplicationArgsValidator validator;
    private InputArgsCapturer capturer;

    public Application(ApplicationArgsValidator validator, InputArgsCapturer capturer) {
        this.validator = validator;
        this.capturer = capturer;
    }

    public static void main(String[] args) {
        new Application(new ApplicationArgsValidator(), new InputArgsCapturer()).run();
    }

    public void run() {
        System.out.println("Application Running...");
        printUsage();

        Canvas2D canvas = null;
        while (true) {
            String[] args = capturer.captureInputArgs();
            switch (args[0].toUpperCase()) {
                case "C":
                    if(!validator.isValid(args)){
                        handleArgumentsError("Invalid arguments specified", args);
                        break;
                    }
                    try {
                        int width = Integer.parseInt(args[1]);
                        int height = Integer.parseInt(args[2]);
                        canvas = new Canvas2D(width, height);
                        System.out.println(canvas);
                    } catch (NumberFormatException e) {
                        handleArgumentsError("Width and/or Height input arguments not numbers", args);
                    } catch (IllegalArgumentException e){
                        handleArgumentsError(e.getMessage(), args);
                    }
                    break;
                case "L":
                    if(!validator.isValid(args)){
                        handleArgumentsError("Invalid arguments specified", args);
                        break;
                    }
                    if (canvas == null){
                        System.err.println(String.format("Can't perform command: %s. Please create a canvas",
                                args[0].toUpperCase()));
                        break;
                    }
                    try {
                        int x1 = Integer.parseInt(args[1]);
                        int y1 = Integer.parseInt(args[2]);
                        int x2 = Integer.parseInt(args[3]);
                        int y2 = Integer.parseInt(args[4]);

                        canvas.acceptDraw(new LineTool(x1, y1, x2, y2), new DrawingStyle('x'));
                        System.out.println(canvas);
                    } catch (NumberFormatException e) {
                        handleArgumentsError("Width and/or Height input arguments not numbers", args);
                    } catch (IllegalArgumentException e){
                        handleArgumentsError(e.getMessage(), args);
                    }
                    break;
                case "R":
                    if(!validator.isValid(args)){
                        handleArgumentsError("Invalid arguments specified", args);
                        break;
                    }
                    if (canvas == null){
                        System.err.println(String.format("Can't perform command: %s. Please create a canvas",
                                args[0].toUpperCase()));
                        break;
                    }
                    try {
                        int x1 = Integer.parseInt(args[1]);
                        int y1 = Integer.parseInt(args[2]);
                        int x2 = Integer.parseInt(args[3]);
                        int y2 = Integer.parseInt(args[4]);

                        canvas.acceptDraw(new RectangleTool(x1, y1, x2, y2), new DrawingStyle('x'));
                        System.out.println(canvas);
                    } catch (NumberFormatException e) {
                        handleArgumentsError("Width and/or Height input arguments not numbers", args);
                    } catch (IllegalArgumentException e){
                        handleArgumentsError(e.getMessage(), args);
                    }
                    break;
                case "Q":
                    System.out.println("Application Stopping...");
                    return;
                default:
                    handleArgumentsError("Unrecognised Option specified", args);
                    break;
            }
        }
    }

    private void handleArgumentsError(String message, String[] args) {
        System.err.println(message + ": " + Arrays.toString(args));
        printUsage();
    }

    private void printUsage() {
        System.out.println("Usage:\n" +
                "   C {width} {height}      e.g: C 20 5      - Creates a canvas\n" +
                "   L {x1} {x2} {x3} {x4}   e.g: L 1 3 7 3   - Draws a line (requires a canvas)\n" +
                "   R {x1} {x2} {x3} {x4}   e.g: R 15 2 20 5 - Draws a rectangle (requires a canvas)\n" +
                "   Q                                        - Quit application\n");
    }

    public static class InputArgsCapturer {
        private Scanner scanner = new Scanner(System.in);

        public String[] captureInputArgs(){
            String line = scanner.nextLine();
            String[] args = line.split("\\s+");
            return args;
        }
    }
}
