package com.deutsche.paint.validator;

public class ApplicationArgsValidator {

    public boolean isValid(String[] args) {
        if(args == null || args.length == 0){
            return false;
        }
        switch (args[0]){
            case "C":
                if (args.length != 3) {
                    return false;
                }
                return isNumber(args[1].toCharArray(), args[2].toCharArray());
            case "L":
            case "R":
                if (args.length != 5) {
                    return false;
                }
                return isNumber(args[1].toCharArray(), args[2].toCharArray(), args[3].toCharArray(), args[4].toCharArray());
            case "Q":
                return true;
            default:
                return false;
        }

    }

    private boolean isNumber(char[]... charArrays) {
        for(char[] charArray : charArrays){
            for(char c : charArray){
                if(c == '-' || c == '+'){
                    continue;
                }
                if(!Character.isDigit(c)){
                    return false;
                }
            }
        }
        return true;
    }
}
