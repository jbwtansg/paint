package com.deutsche.paint.canvas;

import com.deutsche.paint.drawingstyle.DrawingStyle;
import com.deutsche.paint.drawingtool.DrawingTool;

public class Canvas2D {
    private int width;
    private int height;
    private char[][] contents;

    private static final char HEIGHT_EDGE_CHAR = '-';
    private static final char WIDTH_EDGE_CHAR = '|';

    public Canvas2D(int width, int height) {
        if (width < 1) {
            throw new IllegalArgumentException("Width of canvas cannot be less than 1");
        }
        if (height < 1) {
            throw new IllegalArgumentException("Height of canvas cannot be less than 1");
        }

        this.width = width;
        this.height = height;
        initialiseContents(width, height);
    }

    private void initialiseContents(int width, int height){
        this.contents = new char[height + 2][width + 2];
        for(int y = 0; y < height + 2; y += height + 1){
            for(int x = 0; x < width + 2; x++){
                this.contents[y][x] = HEIGHT_EDGE_CHAR;
            }
        }

        for(int y = 0; y < height + 2; y ++){
            for(int x = 0; x < width + 2; x+= width + 1){
                this.contents[y][x] = WIDTH_EDGE_CHAR;
            }
        }
    }

    public Canvas2D acceptDraw(DrawingTool tool, DrawingStyle style) {
        tool.draw(this, style);
        return this;
    }

    public void fill(int x, int y, char symbol){
        if (x > width || x < 1) {
            return;
        }
        if (y > height || y < 1) {
            return;
        }
        contents[y][x] = symbol;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < contents.length; i++) {
            for (int j = 0; j < contents[i].length; j++) {
                if (contents[i][j] == '\u0000') {
                    buffer.append(" ");
                } else {
                    buffer.append(contents[i][j]);
                }
            }
            buffer.append("\n");
        }
        buffer.append("\n");
        return buffer.toString();
    }
}
